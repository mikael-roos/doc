---
title: "Learn some Unix commands"
author: "Mikael Roos"
---

Learn some Unix commands
==============================

Here are a few useful Unix commands when you try to learn your way around the terminal.

| Command | What
|---------|------
| `pwd` | Show the current working directory.
| `ls` | Show all files and directories in the current directory.
| `ls -l` | Show additional details on the files and directories.
| `ls -a` | Show even the hidden files, those starting with a dot `.`.
| `ls -la` | Do it all.
| `mkdir somedir` | Create a new directory named `somedir`.
| `rmdir somedir` | Remove an empty directory named `somedir`.
| `cd somedir` | Change to a sub directory named `somedir`, the directory must exist.
| `cd ..` | Change one directory up in the directory hierarchy.
| `cd` | Change directory to your home directory.
| `touch file.txt` | Create a new file named `file.txt`
| `cat file.txt` | Show the content of the file.
| `more file.txt` | Show the content of the file and paginate its output.
| `rm file.txt` | Remove the file.



<!--
Video serie on the terminal
------------------------------

To few videos...

[Unix shell and the bash terminal - Learn and practice](https://www.youtube.com/watch?v=kialYZs6Oyc&list=PLEtyhUSKTK3gHj087mUjPfXyqMvSy2Rwz)


-->

Learn more on the bash shell
------------------------------

There is an helpful online resource "[Learning the Shell](http://linuxcommand.org/lc3_learning_the_shell.php)" that can be useful to follow.
